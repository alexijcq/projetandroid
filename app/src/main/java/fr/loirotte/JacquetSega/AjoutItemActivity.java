package fr.loirotte.JacquetSega;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

public class AjoutItemActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajout_item);
        Button valider = (Button) findViewById(R.id.validation);
        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText editTextNomItem = findViewById(R.id.name_item);
                String nomItem = editTextNomItem.getText().toString();
                TodoItem.Tags tag;
                if (((RadioButton) findViewById(R.id.faible)).isChecked())
                    tag = TodoItem.Tags.Faible;
                else if (((RadioButton) findViewById(R.id.normal)).isChecked())
                    tag = TodoItem.Tags.Normal;
                else
                    tag = TodoItem.Tags.Important;

                TodoItem item = new TodoItem(tag, nomItem);
                TodoDbHelper.addItem(item, getApplicationContext());

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });

    }

}
